using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    [SerializeField] string _NextLevl;

    Monster[] _Monster;
     

    void OnEnable()
    {
        _Monster = FindObjectsOfType<Monster>();
    }
    // Update is called once per frame
    void Update()
    {
        if(MonsterAllDead())
        {
            GoToNextLevel();
        }
    }

     bool MonsterAllDead()
    {
       foreach(var monster in _Monster)
        {
            if(monster.gameObject.activeSelf)
            {

                return false;
            }                        
        }
        return true;

    }

    void GoToNextLevel()
    {
        Debug.Log("Go To Next" + _NextLevl);
        SceneManager.LoadScene(_NextLevl);
    }
}
