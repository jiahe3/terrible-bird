using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bird : MonoBehaviour
{
    [SerializeField] float _launchForce= 800;
    [SerializeField] float _maxDragDistance = 5;
    Vector2 StartPosition;
    Rigidbody2D RB2;
    

    // Start is called before the first frame update
     void Awake()
    {
        RB2 = GetComponent<Rigidbody2D>();
    }
    void Start()
    {
       
        StartPosition = RB2.position;
        RB2.isKinematic = true;
    }
    void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().color = Color.red;
    }
     void OnMouseUp()
    {
        Vector2 CurrentPosition = RB2.position;
        Vector2 DirectPosition = StartPosition - CurrentPosition;
        DirectPosition.Normalize();
        RB2.isKinematic = false;
        RB2.AddForce(DirectPosition* _launchForce);

        GetComponent<SpriteRenderer>().color = Color.white;
    }
    void OnMouseDrag()
    {
       Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector2 controlposition = mousePosition;
      
        float distance = Vector2.Distance(controlposition, StartPosition);
        if (distance > _maxDragDistance)
        {
            Vector2 direction = controlposition - StartPosition;
            direction.Normalize();
            controlposition = StartPosition +( direction* _maxDragDistance);
        }
        if (controlposition.x > StartPosition.x)
        {
            controlposition = StartPosition;
        }
        RB2.position = controlposition;
    }
    // Update is called once per frame
    void Update()
    {
        
        if (transform.position.x >40)
        {
            RB2.position = StartPosition;
            RB2.isKinematic = true;
            RB2.velocity = Vector2.zero;
        }
    }
     void OnCollisionEnter2D(Collision2D collision)
    {
        StartCoroutine(Delaywaite());
    }

     IEnumerator Delaywaite()
    {
       yield return new WaitForSeconds(4);
        RB2.position = StartPosition;
        RB2.isKinematic = true;
        RB2.velocity = Vector2.zero;
    }
}
