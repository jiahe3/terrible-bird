using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Monster : MonoBehaviour
{
    [SerializeField] Sprite _Dead;
    [SerializeField] ParticleSystem _ParticleSystem;
     bool _hasDead;

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collisiontrue(collision))
        {
           StartCoroutine( Die());
        }
       
           
    }

    private bool collisiontrue(Collision2D collision)
    {
        if(_hasDead)
        {
            return false;
        }
        Bird bird = collision.gameObject.GetComponent<Bird>();
        if (bird != null)
            return true;

        if (collision.contacts[0].normal.y < -0.5)
            return true;
        return false;
    }

    IEnumerator  Die()
    {

        _hasDead = true;    
        _ParticleSystem.Play();
        GetComponent<SpriteRenderer>().sprite = _Dead;
        yield return new WaitForSeconds(1);        
        gameObject.SetActive(false);
    }
}
